<?php
class Wochenplan
{
	private $url = "http://www.oszimt.de/0-schule/termine/2012-13_abc.html";
	public function setUrl($url)
	{
		$this->url = $url;
	}
	public function getTurnusplan()
	{
		$data = file($this->url);
		$month = 0;
		$year = 0;
		$turnus = " ";
		$ret = array();
		foreach($data as $line)
		{
			$editLine = str_replace("ä", "ae", $line);
			if($this->isSubstr($editLine, '<p class="wochenplan">'))
			{
				preg_match("@>\s*([^\s]*)\s([0-9]{4})@", $editLine, &$t);
				if(isset($t[1])&&isset($t[2]))
				{
					$month = $this->getMonth($t[1]);
					$year = $t[2];
				}
			}
			if(preg_match('@<td class="wochenplan">([a-zA-Z])</td>@', $editLine, &$t)) //Change it if u are not in A/B/C Turnus
			{
				if(isset($t[1]))
				{
					$turnus = $t[1];
				}
				else 
				{
					$turnus = "";
				}
			}
			if(preg_match('@<td class="wochenplan">([0-9]*)</td>@', $editLine, &$t))
			{
				//$ret[$turnus][] = $t[1].".".$month.".".$year;
				$day = $t[1];
				if($t[1]<9)
				{
					$day = "0".$t[1];
				}
				$ret[$turnus][] = array("year"=>$year, "month" => $month, "day" => $day);
			}
			

		}
		return $ret;
	}
	private function isSubstr($line, $str, $start = 0)
	{
		if(substr($line, $start, strlen($str))==$str)
		{
			return true;
		}
		return false;
	}
	private function getMonth($str)
	{
		$m = array("Januar"=>"01", "Februar" => "02", "März" => "03", "April" => "04", "Mai" => "05", "Juni" => "06", "Juli" => "07", "August"=>"08", "September" => "09", "Oktober" => "10", "November" => "11", "Dezember" => "12");
		if(isset($m[$str]))
		{
			return $m[$str];
		}
		else
		{
			//echo "Monat ".$str." undefiniert";
			return 3;
		}
	}

}
?>
