<?php
include("getData.class.php");
include("iCalWriter.php");
$w = new Wochenplan();
$plan = $w->getTurnusplan();
$myCal = new iCalWriter();
$myCal->setDownloadOutput();
$myCal->setFileName("test.ics");
$myCal->start();
//var_dump($plan["A"]);
foreach($plan["A"] as $schoolDay)
{
//	echo "<h3>".$schoolDay["day"].".".$schoolDay["month"].".".$schoolDay["year"]."</h3>";
	$myEvent = new iCalEvent();
	$myEvent->setStart($schoolDay["year"], $schoolDay["month"], $schoolDay["day"], 0, 1, "", 1, "08", "00");
	$myEvent->setEnd($schoolDay["year"], $schoolDay["month"], $schoolDay["day"], 0, 1, "", 1, "16", "00");
	$myEvent->setShortDescription("Berufsschule Turnus A");
	$myEvent->setLongDescription("Berufsschule OSZIMT");
	$myEvent->setLocation("Haarlemer Str. 23, 12359 Berlin");
	$myEvent->setGeo(52.457528, 13.451562);
	$myCal->add($myEvent);

}
$myCal->end();
$myCal->setFileName("calender.ics");


//echo $myCal->getOutputMethod();
?>
